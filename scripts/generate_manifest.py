#!/usr/bin/env python3
import json
import os
from os import path

SCRIPT_DIR = path.abspath(path.dirname(__file__))
PROJECT_DIR = path.dirname(SCRIPT_DIR)

NATIVE_DIR = path.join(PROJECT_DIR, "native")
EXECUTOR_PATH = path.join(NATIVE_DIR, "executor.py")
INPUT_MANIFEST_PATH = path.join(NATIVE_DIR, "manifest.json")
OUTPUT_MANIFEST_PATH = path.expanduser("~/.mozilla/native-messaging-hosts/com.namingthingsishard.firefox.ccex.json")

try:
    os.mkdir(path.dirname(OUTPUT_MANIFEST_PATH))
except:
    pass

# Read the manifest template

with open(INPUT_MANIFEST_PATH) as in_manifest:
    manifest = json.load(in_manifest)

manifest["path"] = EXECUTOR_PATH

# Fill and write out the manifest template
with open(OUTPUT_MANIFEST_PATH, "w") as out_manifest:
    json.dump(manifest, out_manifest, indent=2)

print("Wrote manifest to %s" % OUTPUT_MANIFEST_PATH)

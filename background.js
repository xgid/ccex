/*
On startup, connect to the "executor" app.
*/
var port = browser.runtime.connectNative("com.namingthingsishard.firefox.ccex");

/*
Listen for messages from the app.
*/
port.onMessage.addListener((response) => {
    console.log("Received: " + response);
});

/**
 * Send the command to the executor app
 */
browser.contextMenus.onClicked.addListener(function (info, tab) {
    const commandId = info.menuItemId;
    let url = info.mediaType ? info.srcUrl : info.linkUrl;
    if (url) {
        browser.storage.sync.get(commandId).then((commandObject) => {
            commandObject = commandObject[commandId];
            const message = commandObject.commandArray.map((line) => line.replace("$URL$", url));
            port.postMessage(message);
        })
    }
})

// Build the menu of commands
browser.storage.sync.get().then((_storage) => {
    Object.keys(_storage).forEach((commandId) => {
        let commandObject = _storage[commandId];
        browser.contextMenus.create({
            id: commandId,
            title: commandObject.label || commandId,
            contexts: [
                "image",
                "link"
            ]
        });
    })
})
